/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING1, ENDING2 } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    const int maxVelocity = 10;
    const int minVelocity = 8;
    bool pause = false;
    
    GameScreen screen = LOGO;
    
    Color logoColor = RED;
    logoColor.a = 0;
    
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    //PLAYER
    Rectangle player;
    player.width = 20;
    player.height = 100;    
    player.x = 50;
    player.y = screenHeight/2 - player.height/2;

    char *texto;

    int playerSpeedY = 8;
    int playerLife = 5;
    
    //ENEMY
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 100;    
    enemy.x = screenWidth - 50 - enemy.width;
    enemy.y = screenHeight/2 - enemy.height/2;
    
    int enemySpeedY = 6;
    int enemyLife = 5;
    
    //BALL
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
    ballSpeed.x = minVelocity;
    ballSpeed.y = minVelocity;
    
    int ballRadius = 25;

    int iaLinex = screenWidth/1.5;
    
    int secondsCounter = 120;
    
    int framesCounter = 0;          // General pourpose frames counter
    int timeCounter = 10;
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    //VIDA DEL PLAYER
    Rectangle backRect;
    backRect.width = 100;
    backRect.height = 20;    
    backRect.x = 50;
    backRect.y = screenHeight/10 - backRect.height/2;
    int margin = 5;
    Rectangle fillRect = { backRect.x + margin, backRect.y + margin, backRect.width - (2 * margin), backRect.height - (2 * margin)};
    Rectangle lifeRect = fillRect;
    Color lifeColor = YELLOW;
    
    //VIDA DEL ENEMY
    Rectangle backRect2;
    backRect2.width = 100;
    backRect2.height = 20;    
    backRect2.x = screenWidth - 50 - backRect2.width;
    backRect2.y = screenHeight/10 - backRect2.height/2;
    Rectangle fillRect2 = { backRect2.x + margin, backRect2.y + margin, backRect2.width - (2 * margin), backRect2.height - (2 * margin)};
    Rectangle lifeRect2 = fillRect2;
    
    bool win = false;
    bool lose = false;
    
    int winNum = 0;
    int loseNum = 0;
    int drainLife = 1;
    int healLife = 15;
    int healLife2 = 15;
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    Texture2D texture = LoadTexture("resource/Kanji_Muten_Roshi.png");
    Texture2D texture2 = LoadTexture("resource/fondo.png");  
    Texture2D texture3 = LoadTexture("resource/fondo2.png");
    Texture2D texture4 = LoadTexture("resource/fondo3.png");
    Texture2D texture5 = LoadTexture("resource/fondo4.png");
   // Texture2D texture2 = LoadTexture();
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();      // Initialize audio device

    Music dbz = LoadMusicStream("resource/dbz.ogg");
    
    PlayMusicStream(dbz);
    
    Sound boing = LoadSound("resource/bounce.wav");
    Sound exploude = LoadSound("resource/exploud.wav");
    
    SetTargetFPS(60);
    
    bool blink = false;
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        UpdateMusicStream(dbz);

        switch(screen) 
        {
            case LOGO: 
            {
                StopMusicStream(dbz);
                // Update LOGO screen data here!
                framesCounter++;
                if (logoColor.a < 255) logoColor.a++;
                
                if(framesCounter > 300){
                    screen = TITLE;
                    framesCounter = 0;
                }
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                StopMusicStream(dbz);
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
             
                if (framesCounter % 30 == 0)
                {
                   framesCounter = 0;
                   blink = !blink;
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
                
                framesCounter++;
                
            } break;
            case GAMEPLAY:
            { 
            PlayMusicStream(dbz);
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
             if (!pause){
                 ballPosition.x += ballSpeed.x;
                 ballPosition.y += ballSpeed.y;
             } 
             
            
                // TODO: Player movement logic.......................(0.2p)
             if(!pause){
               
                if (IsKeyDown(KEY_Q)){
                    player.y -= playerSpeedY;
                }
           
                if (IsKeyDown(KEY_A)){
                    player.y += playerSpeedY;
                }
                // TODO: Enemy movement logic (IA)...................(1p)
                if (IsKeyDown(KEY_RIGHT)){
                    iaLinex += enemySpeedY;
                }
           
                if (IsKeyDown(KEY_LEFT)){
                    iaLinex -= enemySpeedY;
                }
              }
              if( ballPosition.x > iaLinex){
                    if(ballPosition.y > enemy.y){
                        enemy.y += enemySpeedY;
                    }
                    if(ballPosition.y < enemy.y){
                       enemy.y -= enemySpeedY;
                    } 
              }     
              if (player.y < 0){
                  player.y = 0;
              }
              if (player.y > (screenHeight - player.height)){
                  player.y = screenHeight - player.height;
              }
              if (enemy.y < 0){
                  enemy.y = 0;
              }
              if (enemy.y > (screenHeight - enemy.height)){
                  enemy.y = screenHeight - enemy.height;
              }
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if(CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                   if(ballSpeed.x<0){
                      PlaySound(boing); 
                      if(abs(ballSpeed.x)<maxVelocity){
                         ballSpeed.x *=-1;
                         ballSpeed.y *= 1;
                      }else{
                         ballSpeed.x *=-1;
                      }
                   }
                }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                   if(ballSpeed.x>0){
                      PlaySound(boing); 
                      if(abs(ballSpeed.x)<maxVelocity){
                         ballSpeed.x *=-1;
                         ballSpeed.y *= 1;
                      }else{
                         ballSpeed.x *=-1;
                      }
                   }
                }
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if((ballPosition.y > screenHeight - ballRadius) || (ballPosition.y < ballRadius) ){
                    PlaySound(boing);
                    ballSpeed.y *=-1;
                 }
                // TODO: Life bars decrease logic....................(1p)
                if(ballPosition.x > screenWidth - ballRadius){
                   PlaySound (exploude);
                   lifeRect2.width -= healLife2;
                   ballPosition.x = screenWidth/2;
                   ballPosition.y = screenHeight/2;
                   ballSpeed.x = -minVelocity;
                   ballSpeed.y = minVelocity;
            
           
              }else if(ballPosition.x < ballRadius){
                       PlaySound (exploude);
                       lifeRect.width -= healLife;   
                       ballPosition.x = screenWidth/2;
                       ballPosition.y = screenHeight/2;
                       ballSpeed.x = minVelocity;
                       ballSpeed.y = minVelocity;
          
              } 
                // TODO: Time counter logic..........................(0.2p)
                if (secondsCounter > 0){
                    framesCounter++;
                    if (framesCounter == 60){
                        secondsCounter--;
                        framesCounter = 0;
                    }
                }
                if (secondsCounter == 0)
                {
                 screen = ENDING1;
                }
                    
                // TODO: Game ending logic...........................(0.2p)
                else if (lifeRect.width <= 0)// eliminar la victoria
                {
                  ++loseNum;
                  lifeRect.width = fillRect.width;
                  screen = ENDING1;
                }
                else if (lifeRect2.width <= 0)// eliminar la victoria
                {
                  ++winNum;
                  lifeRect2.width = fillRect2.width;
                  screen = ENDING2;
                }
                    
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                    pause = !pause;
                }
            } break;
            case ENDING1: 
            {
                StopMusicStream(dbz);
                // Update END screen data here!
                if(IsKeyPressed(KEY_R)){
                    secondsCounter = 120;
                    screen = GAMEPLAY;
                }
                if(IsKeyPressed(KEY_F)){
                    CloseWindow();
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            case ENDING2: 
            {
                StopMusicStream(dbz);
                // Update END screen data here!
                if(IsKeyPressed(KEY_R)){
                    
                    secondsCounter = 120;
                    screen = GAMEPLAY;
                }
                if(IsKeyPressed(KEY_F)){
                    CloseWindow();
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, logoColor);
                    //DrawText("Dragon", screenWidth/2 - MeasureText("Dragon", 80)/2, screenHeight/2, 80, logoColor); 
                    // TODO: Draw Logo...............................(0.2p)

                } break;
                case TITLE: 
                {
                    DrawTexture(texture2, 0, 0, WHITE);
                    // Draw TITLE screen here!
                    DrawText("FINAL PONG Z", screenWidth/2 - MeasureText("FINAL PONG Z", 60)/2, screenHeight/4, 60, BLACK);
                    // TODO: Draw Title..............................(0.2p)
                    if (blink) DrawText("Press Enter", screenWidth/2 - MeasureText("Press Enter", 30)/2, screenHeight - 100, 30, BLUE);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    ClearBackground(BLUE);
                    DrawTexture(texture4, 0, 0, WHITE);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(enemy, DARKBLUE);
                    DrawRectangleRec(player, ORANGE);
                    DrawCircleV(ballPosition, ballRadius, YELLOW);
                    DrawLine(iaLinex,0,iaLinex, screenHeight,RED);
                    
                    texto = FormatText("%d",secondsCounter);
                    
                    DrawText ( texto, (screenWidth - MeasureText(texto, 60))/2 ,10, 50, BLACK);
                   // DrawText("%d",secondsCounter, (screenWidth - MeasureText("%d", 40))/2, 10, 40,RED);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                   // DrawText("Enemy",screenWidth - MeasureText("Enemy",40)-5,10,40,RED);
                   // DrawText("Player",5,10,40, RED);
                    DrawRectangleRec (backRect, BLACK);
                    DrawRectangleRec (fillRect, RED);
                    DrawRectangleRec (lifeRect, lifeColor);
                    
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect2, lifeColor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if(pause){
                       DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                       DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, RED);
                    }
                 } break;
                case ENDING1: //Player lose
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                  DrawTexture(texture5, 0, 0, WHITE);
                  DrawText("YOU LOSE", (screenWidth - MeasureText("YOU LOSE", 60))/2, screenHeight/2 - (screenHeight/4), 60, RED);
                  DrawText("PRESS R TO RETRY", (screenWidth - MeasureText("PRESS R TO RETRY", 40))/2, screenHeight/2 + (screenHeight/5), 40, PURPLE);
                  DrawText("PRESS F TO FINAL THIS GAME", (screenWidth - MeasureText("PRESS F TO FINAL THIS GAME", 40))/2, screenHeight/2 + (screenHeight/3), 40, PURPLE);
            
                } break;
                case ENDING2://Enemy lose 
                {     
                  DrawTexture(texture3, 0, 0, WHITE);
                  DrawText("YOU WIN!", (screenWidth - MeasureText("YOU WIN!", 60))/2, screenHeight/2 - (screenHeight/4), 60, GREEN);
                  DrawText("PRESS R TO RETRY", (screenWidth - MeasureText("PRESS R TO RETRY", 40))/2, screenHeight/2 + (screenHeight/5), 40, PURPLE);
                  DrawText("PRESS F TO FINAL THIS GAME", (screenWidth - MeasureText("PRESS F TO FINAL THIS GAME", 40))/2, screenHeight/2 + (screenHeight/3), 40, PURPLE);
            
                }
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadSound(boing);
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(texture);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}